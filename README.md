### How to run

clone project and switch to ```feature/implement-entity-framework-persistence``` branch

1. With dotnet core 3.1 installed

```
cd src/storytel.api
dotnet restore
dotnet run
```

application will start: 
on https://localhost:8081 and http://localhost:8080

2. With docker
   Build image from root of project

```
docker build . -t storytel-api
```

Run docker image

```
docker run -d -p 8081:80 --name storytel-api storytel-api
```

https://localhost:8081


### Project structure

    src/storytel.api dotnet core webapi project
    tests/storytel.api xunit test project


### Giving the project a spin

messageboard.http requires vs code extension: https://marketplace.visualstudio.com/items?itemName=humao.rest-client

not installed you can still use it as documentation: api/message post, put and delete api requires a logged in user.

### Additional comments

I was considering F# briefly, but I went for C#. I enjoy writing business logic in F#, but the assignment focuses mostly on Frameworks and I feel less comfortable
setting it up in F#. I used the most basic ```dotnet new webapi``` project. I thought about using it with auth setup, but I would rather have a bit more control for an assignment like this.

I thought it would be a nice idea to do my work in a couple of branches. That also gives me the opportunity that give some thoughts about some of my choices.

So I would invite you to have a look at:

https://gitlab.com/24292da3-f12f-46d8-8a81-f59a522ad286/storytel/-/merge_requests/1 and https://gitlab.com/24292da3-f12f-46d8-8a81-f59a522ad286/storytel/-/merge_requests/2

The app works, but it is still a big rough on the edges. I think there is room for plenty of improvement.

1. Only the happy path is supported. I think validation is an important part of an application. There is not a good separation of domain and frontend. With more time I would use something like https://fluentvalidation.net/ to make sure the request passes all the business requirements. Also most type signatures in my application are lie. The application would improve with Result and option types to make it more clear to the caller what the methods return.
2. Not every part of the application is tested. I'm a huge fan of tests. It's part of the way I do development. But there is not much business logic to be tested in the application. Although I still think i've should have written tests for the service and the repository. Think one of my previous assignments I did for the company I worked for showcases this better. https://gitlab.com/onwijsbebeerte/Infi
3. The domain is poorly reflected in the architecture. There are plenty of folders with a few files which makes it feel the application is made in 2010 instead of 2020. There are 2 parts in this domain. Messages and authentication. This should be super obvious from the structure.
4. Although I'm a bigger fan of descriptive naming than of xml comments. They are currently missing and I do think they are very useful when you are writing a library with users or want to use something like swagger then know how to parse it.
5. App-secrets, password and configuration should not be part of the repository.
6. There is no CICD, no tests being run automatically and deployment to test environments